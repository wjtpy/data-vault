# **An example of Data Vault 1.0 model for simple data**

Link to source: https://danischnider.wordpress.com/2015/11/12/loading-dimensions-from-a-data-vault-model/

Requirements:

1.	Database client (In this example I recommend pgAdmin https://www.pgadmin.org/download/ or alternatively DBeaver https://dbeaver.io/download/).
2.	If you chose DBeaver, you need JDBC driver for PGSQL from here https://jdbc.postgresql.org/download.html.
---

## 1. Creating tables

Before this step you need create a new schema (in my example it calls *data_vault*).

First of all you have to create required tables for this example. Below you can take DDL for tables:

### HUB → H_CUSTOMER:
```
CREATE TABLE data_vault."H_CUSTOMER"
(
    "H_CUSTOMER_SID" integer NOT NULL DEFAULT nextval('data_vault."H_CUSTOMER_H_CUSTOMER_SID_seq"'::regclass),
    "CUSTOMER_NUMBER" character varying(15) COLLATE pg_catalog."default" NOT NULL,
    "LOAD_DATE" date,
    "RECORD_SOURCE" character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT "H_CUSTOMER_pkey" PRIMARY KEY ("H_CUSTOMER_SID")
)

TABLESPACE pg_default;

ALTER TABLE data_vault."H_CUSTOMER"
    OWNER to postgres;
```
---
### SATELITTE → S_CUSTOMER_MAIN:
```
CREATE TABLE data_vault."S_CUSTOMER_MAIN"
(
    "H_CUSTOMER_SID" integer NOT NULL,
    "LOAD_DATE" date NOT NULL,
    "FIRST_NAME" character varying(25) COLLATE pg_catalog."default",
    "LAST_NAME" character varying(25) COLLATE pg_catalog."default",
    "DATE_OF_BIRTH" character varying(10) COLLATE pg_catalog."default",
    "GENDER" character varying(10) COLLATE pg_catalog."default",
    "RECORD_SOURCE" character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT "S_CUSTOMER_MAIN_pkey" PRIMARY KEY ("LOAD_DATE", "H_CUSTOMER_SID"),
    CONSTRAINT "H_CUSTOMER_SID_FK" FOREIGN KEY ("H_CUSTOMER_SID")
        REFERENCES data_vault."H_CUSTOMER" ("H_CUSTOMER_SID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE data_vault."S_CUSTOMER_MAIN"
    OWNER to postgres;
```
### SATELITTE → S_CUSTOMER_ADDRESS:
```
CREATE TABLE data_vault."S_CUSTOMER_ADDRESS"
(
    "H_CUSTOMER_SID" integer NOT NULL,
    "LOAD_DATE" date NOT NULL,
    "STREET" character varying(25) COLLATE pg_catalog."default",
    "ZIP_CODE" character varying(10) COLLATE pg_catalog."default",
    "CITY" character varying(25) COLLATE pg_catalog."default",
    "STATE" character varying(25) COLLATE pg_catalog."default",
    "COUNTRY" character varying(25) COLLATE pg_catalog."default",
    "RECORD_SOURCE" character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT "S_CUSTOMER_ADDRESS_pkey" PRIMARY KEY ("H_CUSTOMER_SID", "LOAD_DATE"),
    CONSTRAINT "H_CUSTOMER_SID_FKEY" FOREIGN KEY ("H_CUSTOMER_SID")
        REFERENCES data_vault."H_CUSTOMER" ("H_CUSTOMER_SID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE data_vault."S_CUSTOMER_ADDRESS"
    OWNER to postgres;
```
### SATELITTE → S_CUSTOMER_ONLINE:
```
CREATE TABLE data_vault."S_CUSTOMER_ONLINE"
(
    "H_CUSTOMER_SID" integer NOT NULL,
    "LOAD_DATE" date NOT NULL,
    "EMAIL_HOME" character varying(50) COLLATE pg_catalog."default",
    "EMAIL_WORK" character varying(50) COLLATE pg_catalog."default",
    "RECORD_SOURCE" character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT "S_CUSTOMER_ONLINE_pkey" PRIMARY KEY ("H_CUSTOMER_SID", "LOAD_DATE"),
    CONSTRAINT "H_CUSTOMER_SID_FKEY" FOREIGN KEY ("H_CUSTOMER_SID")
        REFERENCES data_vault."H_CUSTOMER" ("H_CUSTOMER_SID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE data_vault."S_CUSTOMER_ONLINE"
    OWNER to postgres;
```
### SATELITTE → S_CUSTOMER_PHONE:
```
CREATE TABLE data_vault."S_CUSTOMER_PHONE"
(
    "H_CUSTOMER_SID" integer NOT NULL,
    "LOAD_DATE" date NOT NULL,
    "PHONE_HOME" character varying(15) COLLATE pg_catalog."default",
    "PHONE_WORK" character varying(15) COLLATE pg_catalog."default",
    "PHONE_MOBILE" character varying(15) COLLATE pg_catalog."default",
    "RECORD_SOURCE" character varying(25) COLLATE pg_catalog."default",
    CONSTRAINT "S_CUSTOMER_PHONE_pkey" PRIMARY KEY ("H_CUSTOMER_SID", "LOAD_DATE"),
    CONSTRAINT "H_CUSTOMER_SID_FKEY" FOREIGN KEY ("H_CUSTOMER_SID")
        REFERENCES data_vault."H_CUSTOMER" ("H_CUSTOMER_SID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE data_vault."S_CUSTOMER_PHONE"
    OWNER to postgres;
```
### SATELITTE (Time in Point table) → S_CUSTOMER_PIT:
```
CREATE TABLE data_vault."S_CUSTOMER_PIT"
(
    "H_CUSTOMER_SID" integer NOT NULL,
    "LOAD_DATE" date NOT NULL,
    "LOAD_END_DATE" date,
    "S1_LOAD_DATE" date,
    "S2_LOAD_DATE" date,
    "S3_LOAD_DATE" date,
    "S4_LOAD_DATE" date,
    CONSTRAINT "S_CUSTOMER_PIT_pkey" PRIMARY KEY ("H_CUSTOMER_SID", "LOAD_DATE"),
    CONSTRAINT "H_CUSTOMER_SID_FKEY" FOREIGN KEY ("H_CUSTOMER_SID")
        REFERENCES data_vault."H_CUSTOMER" ("H_CUSTOMER_SID") MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE data_vault."S_CUSTOMER_PIT"
    OWNER to postgres;
```
## 2. Inserting data into PIT table

Next, you’ll add rows into *S_CUSTOMER_PIT* using a query below:
###
```
INSERT INTO data_vault."S_CUSTOMER_PIT"
("H_CUSTOMER_SID",
 "LOAD_DATE",
 "S1_LOAD_DATE",
 "S2_LOAD_DATE",
 "S3_LOAD_DATE",
 "S4_LOAD_DATE")
WITH "LOAD_DATES" AS (
 SELECT "H_CUSTOMER_SID", "LOAD_DATE" FROM data_vault."S_CUSTOMER_MAIN"
 UNION
 SELECT "H_CUSTOMER_SID", "LOAD_DATE" FROM data_vault."S_CUSTOMER_ADDRESS"
 UNION
 SELECT "H_CUSTOMER_SID", "LOAD_DATE" FROM data_vault."S_CUSTOMER_ONLINE"
 UNION
 SELECT "H_CUSTOMER_SID", "LOAD_DATE" FROM data_vault."S_CUSTOMER_PHONE"
)
SELECT
 LD."H_CUSTOMER_SID",
 LD."LOAD_DATE",
 LEAD(LD."LOAD_DATE") OVER
 (PARTITION BY LD."H_CUSTOMER_SID" ORDER BY LD."LOAD_DATE") LOAD_END_DATE,
 MAX(S1."LOAD_DATE") OVER
 (PARTITION BY LD."H_CUSTOMER_SID" ORDER BY LD."LOAD_DATE") S1_LOAD_DATE,
 MAX(S2."LOAD_DATE") OVER
 (PARTITION BY LD."H_CUSTOMER_SID" ORDER BY LD."LOAD_DATE") S2_LOAD_DATE,
 MAX(S3."LOAD_DATE") OVER
 (PARTITION BY LD."H_CUSTOMER_SID" ORDER BY LD."LOAD_DATE") S3_LOAD_DATE,
 MAX(S4."LOAD_DATE") OVER
 (PARTITION BY LD."H_CUSTOMER_SID" ORDER BY LD."LOAD_DATE") S4_LOAD_DATE
FROM "LOAD_DATES" LD
LEFT JOIN data_vault."S_CUSTOMER_MAIN" S1
ON (S1."H_CUSTOMER_SID" = LD."H_CUSTOMER_SID" AND S1."LOAD_DATE" = LD."LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ADDRESS" S2
ON (S2."H_CUSTOMER_SID" = LD."H_CUSTOMER_SID" AND S2."LOAD_DATE" = LD."LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ONLINE" S3
ON (S3."H_CUSTOMER_SID" = LD."H_CUSTOMER_SID" AND S3."LOAD_DATE" = LD."LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_PHONE" S4
ON (S4."H_CUSTOMER_SID" = LD."H_CUSTOMER_SID" AND S4."LOAD_DATE" = LD."LOAD_DATE");
```

## 3. Creating views 

Now, you'll create two views:

*    V_CUSTOMER_CURR - for current data from all of tables
*    V_CUSTOMER_HIST - for historical data from all of tables

In this case, we use three additional functions:

*    *LEAD* → provides access to a row that follows the current row at a specified physical offset. Function is very useful for comparing the vaule of current row with a value of a row that following a current row.
*    *PARTITION* → refers to spliting what is logically one large table into smaller physical pieces.
*    *COALESCE* → accepts an unlimited number of arguments. It returns a first arguments that is not null. If all arguments are null, function will return null.

### VIEW → V_CUSTOMER_CURR:
```
CREATE OR REPLACE VIEW data_vault."V_CUSTOMER_CURR" AS
SELECT
 HUB."H_CUSTOMER_SID",
 HUB."CUSTOMER_NUMBER",
 S1."FIRST_NAME",
 S1."LAST_NAME",
 S1."DATE_OF_BIRTH",
 S1."GENDER",
 S2."STREET",
 S2."ZIP_CODE",
 S2."CITY",
 S2."STATE",
 S2."COUNTRY",
 S3."EMAIL_HOME",
 S3."EMAIL_WORK",
 S4."PHONE_HOME",
 S4."PHONE_WORK",
 S4."PHONE_MOBILE"
FROM data_vault."S_CUSTOMER_PIT" PIT
JOIN data_vault."H_CUSTOMER" HUB ON (HUB."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID")
LEFT JOIN data_vault."S_CUSTOMER_MAIN" S1
 ON (S1."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S1."LOAD_DATE" = PIT."S1_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ADDRESS" S2
 ON (S2."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S2."LOAD_DATE" = PIT."S2_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ONLINE" S3
 ON (S3."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S3."LOAD_DATE" = PIT."S3_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_PHONE" S4
 ON (S4."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S4."LOAD_DATE" = PIT."S4_LOAD_DATE")
 WHERE PIT."LOAD_END_DATE" IS NULL;
```

### VIEW → V_CUSTOMER_HIST:
```
CREATE OR REPLACE VIEW data_vault."V_CUSTOMER_HIST" AS
SELECT
 HUB."H_CUSTOMER_SID",
 HUB."CUSTOMER_NUMBER",
 PIT."LOAD_DATE" VALID_FROM,
 COALESCE(PIT."LOAD_DATE", TO_DATE('31.12.9999', 'DD.MM.YYYY')) VALID_TO,
 S1."FIRST_NAME",
 S1."LAST_NAME",
 S1."DATE_OF_BIRTH",
 S1."GENDER",
 S2."STREET",
 S2."ZIP_CODE",
 S2."CITY",
 S2."STATE",
 S2."COUNTRY",
 S3."EMAIL_HOME",
 S3."EMAIL_WORK",
 S4."PHONE_HOME",
 S4."PHONE_WORK",
 S4."PHONE_MOBILE"
FROM data_vault."S_CUSTOMER_PIT" PIT
JOIN data_vault."H_CUSTOMER" HUB ON (HUB."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID")
LEFT JOIN data_vault."S_CUSTOMER_MAIN" S1
 ON (S1."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S1."LOAD_DATE" = PIT."S1_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ADDRESS" S2
 ON (S2."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S2."LOAD_DATE" = PIT."S2_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_ONLINE" S3
 ON (S3."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S3."LOAD_DATE" = PIT."S3_LOAD_DATE")
LEFT JOIN data_vault."S_CUSTOMER_PHONE" S4
 ON (S4."H_CUSTOMER_SID" = PIT."H_CUSTOMER_SID" AND S4."LOAD_DATE" = PIT."S4_LOAD_DATE");
```

---
## ERD Model

![Alt text](https://i.ibb.co/5j01JQn/data-vault.png)


